# Simple Shell

## Project Description

## Project path
-   **Command Prompt**: Implement a command prompt that displays a prompt (e.g., $) and waits for user input.

-   **Read User Input**: Read and parse user input from the command prompt.

-   **Execute Commands**: Execute simple commands entered by the user. You can use functions like fork(), exec(), and wait() to create child processes and execute commands.

-   **Handle Built-in Commands**: Implement built-in commands like cd, exit, and help within your shell program.

-   **Support for Pipes**: Add support for simple command pipelines using the pipe (|) operator.

-   **Handle Background Processes**: Allow users to run commands in the background by supporting the & operator.

-   **Redirection**: Implement support for input/output redirection (<, >) to allow users to redirect input from files and output to files.

-   **Signals Handling**: Implement signal handling to handle interrupts like Ctrl+C gracefully.

-   **Error Handling**: Include error handling for various scenarios such as command not found, file not found, etc.

-   **Customization**: Optionally, you can add features like customizable prompt, command history, and command auto-completion.

## useful c programmin stuff
**unistd.h**:
unistd.h stands for "Unix Standard". It provides access to various POSIX (Portable Operating System Interface) system calls, constants, and types.
    Common functions and constants provided by unistd.h include:
        Process handling: fork(), exec(), exit(), getpid(), getppid(), etc.
        File and I/O operations: open(), read(), write(), close(), etc.
        Miscellaneous: sleep(), getcwd(), chdir(), etc.

sys/types.h:
    sys/types.h defines various data types used by system calls and other header files.
    Common types defined by sys/types.h include:
        pid_t: Data type used to represent process IDs.
        size_t: Unsigned integer type used for sizes of objects.
        uid_t and gid_t: Data types for user and group IDs, respectively.
        Other types like off_t, mode_t, etc., used for file operations and permissions.

sys/wait.h:
sys/wait.h provides declarations related to process management and status reporting.
Common functions and macros provided by sys/wait.h include:
    wait(): Suspends execution of the calling process until a child process terminates.
    waitpid(): Similar to wait(), but allows specifying which child process to wait for.
    Macros for interpreting process exit status, such as WIFEXITED(), WEXITSTATUS(), etc.