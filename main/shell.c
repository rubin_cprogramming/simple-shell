/*
    main file of simple shell project 
*/


#include "shell.h"

int main(){
   
    char *input = malloc(MAX_INPUT_SIZE * sizeof(char));
    if (input == NULL){
        printf("Error allocating memory!!! Exiting program");
        return 1;
    }

    while (1){

        char cwd[PATH_MAX];
        if (getcwd(cwd, sizeof(cwd)) == NULL) {
            perror("getcwd");
            exit(EXIT_FAILURE);
        }
        printf("%s %s", cwd, PROMPT);

        if (fgets(input, MAX_INPUT_SIZE, stdin) == NULL){
            perror("fgets");
            exit(EXIT_FAILURE);
        }
        
        input[strcspn(input, "\n")] = '\0';

        executeCommand(input);
    }
    

    free(input);
    return 0;
}


