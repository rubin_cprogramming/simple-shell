/*
    execute command file
*/

#include "shell.h"

void executeCommand(char *command) {
    printf("Command entered: %s\n", command);

    char **args = malloc(ARGS_MAX * sizeof(char *));
    if (args == NULL) {
        printf("Memory allocation failed!!!");
        return;
    }

    int argCount = 0;
    char *token = strtok(command, " ");
    while (token != NULL && argCount < ARGS_MAX - 1) {
        // Allocate memory for the token
        args[argCount] = malloc(strlen(token) + 1); // Add 1 for the null terminator
        if (args[argCount] == NULL) {
            printf("Memory allocation failed!!!");
            // Free previously allocated memory before returning
            for (int i = 0; i < argCount; i++) {
                free(args[i]);
            }
            free(args);
            return;
        }
        // Copy the token into the allocated memory
        strcpy(args[argCount], token);
        argCount++;
        token = strtok(NULL, " ");
    }
    args[argCount] = NULL;

     // Check if the command is a built-in command
    if (args[0] != NULL && strcmp(args[0], "cd") == 0) {
        // Handle cd command
        if (args[1] == NULL) {
            // Change to home directory if no directory is provided
            if (chdir(getenv("HOME")) != 0) {
                perror("cd");
            }
        } else {
            // Change to the specified directory
            if (chdir(args[1]) != 0) {
                perror("cd");
            }
        }
    } else if (args[0] != NULL && strcmp(args[0], "exit") == 0) {
        // Handle exit command
        exit(EXIT_SUCCESS);
    } else if (args[0] != NULL && strcmp(args[0], "help") == 0) {
        // Handle help command
        printf("Simple Shell.\n");
        printf("Available commands: \n");
        printf("cd [directory]: Change directory \n");
        printf("exit: Exit the Shell \n");
        printf("help: Display the help message \n");
    } else {
        // Execute external command
        pid_t pid = fork();
        if (pid < 0) {
            perror("fork");
            exit(EXIT_FAILURE);
        } else if (pid == 0) {
            if (execvp(args[0], args) < 0) {
                perror("execvp");
                exit(EXIT_FAILURE);
            }
        } else {
            wait(NULL);
        }
    }

    // Free allocated memory
    for (int i = 0; i < argCount; i++) {
        free(args[i]);
    }
    free(args);
}

