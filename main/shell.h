/*
    Header file of simple shell project
*/

#ifndef SHELL_H
#define SHELL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_INPUT_SIZE 1024
#define PROMPT "$ "
#define ARGS_MAX 15
#define PATH_MAX 1000

void executeCommand(char *command);
void handleBuiltInCommands(char **args);

#endif /* SHELL_H*/